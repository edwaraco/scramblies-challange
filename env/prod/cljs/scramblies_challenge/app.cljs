(ns scramblies-challenge.app
  (:require [scramblies-challenge.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
