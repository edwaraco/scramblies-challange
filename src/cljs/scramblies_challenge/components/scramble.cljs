(ns scramblies-challenge.components.scramble
  (:require [reagent.core :as r]
            [ajax.core :refer [GET POST]]))

(defn update-scramble-response [session]
  (fn [response] (swap! session assoc :scramble (:scramble response))))

(defn scramble? [scramble-session fn-callback]
  (when-not (or (empty? (:str-1 @scramble-session)) (empty? (:str-2 @scramble-session)))
    (GET (str "/api/scramble")
         {:params {:str_1 (:str-1 @scramble-session) :str_2 (:str-2 @scramble-session)}
          :response-format :json
          :keywords? true
          :handler fn-callback})))

(defn input-color [input-value]
  (cond
    (nil? input-value) nil
    (empty? input-value) "error"
    :else "success"))

(defn input-element
  "An input element which updates its value on change"
  [id name type kw session]
  [:input {:id id
           :name name
           :class ["form-control"]
           :type type
           :required "This field is required."
           :value (kw @session)
           :on-change #(swap! session assoc kw (-> % .-target .-value))}])

(defn input-and-prompt
  "Creates an input box and a prompt box that appears above the input when the input comes into focus. Also throws in a little required message"
  [label-value input-name input-type state-elements prompt-element required?]
  (let [str-input-name (str input-name)]
    [:div.form-group
     [:div.row
      [:label.col-lg-12 prompt-element]]
     [:div.row
      [:div.col-lg-12 [input-element str-input-name str-input-name input-type input-name state-elements]]]
     [:div.row
      (if (and required? (= "" (@state-elements input-name)))
        [:span.col-lg-12 "Field is required!"]
        [:span])]]))

(defn process-response [scramble-session]
  (when-not (nil? (:scramble @scramble-session))
    [:div#result {:class "row"}
     [:span (str "The two strings are "
                 (when (false? (:scramble @scramble-session)) "un")
                 "scramble")]]))

(defn build-scramble-form [scramble-session]
  [:div.row
   [:div {:class "col-lg-12"}
    [:div.row
     (input-and-prompt "String 1:" :str-1 "text" scramble-session "Please type the string 1..." true)]
    [:div.row
     (input-and-prompt "String 2:" :str-2 "text" scramble-session "Please type the string 2..." true)]
    [:div.row
     [:div.col-lg-12
      [:button {:on-click #(scramble? scramble-session
                                      (update-scramble-response scramble-session)) :class "btn-primary"} "Check"]]]
    [process-response scramble-session]]])

(defn build-scramble-form-old [scramble-session]
  [:div.row
   [:div {:class "col-lg-12"}
    [:div.row
     [:div {:class "col-lg-2"}
      [:label {:for "str-1"} "Str 1:"]]
     [:div {:class "col-lg-4"}
      (input-element "str-1" "str-1" "text" :str-1 scramble-session)]]
    [:div.row
     [:div {:class "col-lg-2"}
      [:label {:for "str-2"} "Str 2:"]]
     [:div {:class "col-lg-4"}
      (input-element "str-2" "str-2" "text" :str-2 scramble-session)]]
    [:div.row
     [:div {:class "col-lg-6"}
      [:button {:on-click #(scramble? scramble-session
                                      (update-scramble-response scramble-session)) :class "btn-primary"} "Check"]]]
    [:hr]
    [process-response scramble-session]]])
