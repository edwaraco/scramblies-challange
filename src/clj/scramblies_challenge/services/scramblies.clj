(ns scramblies-challenge.services.scramblies)

(defn scramble
  [freq-str-1 [key-str-2 val-str-2]]
  (when (or (nil? (get freq-str-1 key-str-2))
            (< (get freq-str-1 key-str-2) val-str-2))
    key-str-2))

(defn scramble?
  "Returns true if a portion of str1 characters can be rearranged to match str2,
   otherwise returns false"
  [str-1 str-2]
  (let [freq-str-1 (frequencies str-1)
        freq-str-2 (frequencies str-2)]
    (nil? (some (partial scramble freq-str-1)  freq-str-2))))
