(ns scramblies-challenge.components.scramble-test
  (:require [cljs.test :refer-macros [async is deftest testing use-fixtures run-tests]]
            [reagent.core :as r]
            [scramblies-challenge.components.scramble :as sc]))

(deftest process-response-contains-scramble-test
  (let [scramble-session (r/atom {:scramble true})]
    (is (seq (sc/process-response scramble-session)))))

(deftest process-response-doesnt-contain-scramble-test
  (let [scramble-session (r/atom {:scramble nil})]
    (is (empty? (sc/process-response scramble-session)))))

(deftest scramble-test
  (let [scramble-session (r/atom {:str_1 "testing" :str_2 "git"})]
    (with-redefs [ajax.core/GET (fn [url props]
                                  (is (= "/api/scramble" url))
                                  ((:handler props) {:status 200 :scramble true}))]
      (sc/scramble? scramble-session
                    (fn [response]
                      (is (= 200 (:status response)))
                      (is (true? (:scramble response))))))))

(deftest update-scramble-session-test
  (let [scramble-session (r/atom {:scramble nil})]
    ((sc/update-scramble-response scramble-session) {:scramble false})
    (is (false? (:scramble @scramble-session)))))
